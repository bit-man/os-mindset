#!/bin/bash

__PRG=$(basename $0)

function showMsg() {
    echo "$__PRG: $1"
}

function showErr() {
    showMsg "ERROR: $*"
}

function testShow() {
    [[ -z $__EW_TEST ]] && return

    while [[ $1 ]]; do
        echo "${__PRG}: TEST: $1"
        shift
    done
}

function OKorExit() {
	__rc=$?
    [[ $__rc -ne 0 ]] && \
    	showErr $* && testShow "STATUS=ERROR" && exit $__rc
}

function detectOS() {
  case $OSTYPE in
    darwin*)
        echo macosx
      ;;
    linux*)
        echo debian
      ;;
  esac
}
