#!/bin/bash

# associative arrays (should work in bash 3.1 or above)
# https://gist.github.com/izabera/2d1f2f63500efc8f6ec2

djb2 () {
  local tmp i LANG=C
  hash=5381
  while printf -v tmp %d "'${1:i++:1}"; (( tmp )); do
    (( hash = hash * 33 + tmp ))
  done
  printf -v hash %020d_ "$hash"
  hash=${hash/#-/_}
}

# isset arrayname key
isset () {
  local tmp i LANG=C
  [[ $# == 2 && $1 =~ ^[[:alnum:]]+$ ]] || return 2
  djb2 "$2"
  if eval "[[ \${hashtable$hash$1+set} ]]"; then
    eval "tmp=(\"\${hashtable$hash$1[@]}\")"
    for (( i = 0; i < "${#tmp[@]}"; i+=2 )) do
      [[ ${tmp[i]} == "$2" ]] && return
    done
  else
    return 1
  fi
}

# setvalue arrayname key value
setvalue () {
  local tmp i LANG=C
  [[ $# == 3 && $1 =~ ^[[:alnum:]]+$ ]] || return 2
  djb2 "$2"
  if eval "[[ \${hashtable$hash$1+set} ]]"; then
    eval "tmp=(\"\${hashtable$hash$1[@]}\")"
    for (( i = 0; i < "${#tmp[@]}"; i+=2 )) do
      if [[ ${tmp[i]} == "$2" ]]; then
        eval "hashtable$hash$1[i+1]=\$3"
        return
      fi
    done
  fi
  eval "hashtable$hash$1+=(\"\$2\" \"\$3\")"
}

# getvalue arrayname key
getvalue () {
  local tmp i LANG=C
  [[ $# == 2 && $1 =~ ^[[:alnum:]]+$ ]] || return 2
  djb2 "$2"
  if eval "[[ \${hashtable$hash$1+set} ]]"; then
    eval "tmp=(\"\${hashtable$hash$1[@]}\")"
    for (( i = 0; i < "${#tmp[@]}"; i+=2 )) do
      if [[ ${tmp[i]} == "$2" ]]; then
        return=${tmp[i+1]}
        return
      fi
    done
  else
    return 1
  fi
}

# unsetvalue arrayname key
unsetvalue () {
  local tmp i LANG=C
  [[ $# == 2 && $1 =~ ^[[:alnum:]]+$ ]] || return 2
  djb2 "$2"
  if eval "[[ \${hashtable$hash$1+set} ]]"; then
    eval "tmp=(\"\${hashtable$hash$1[@]}\")"
    for (( i = 0; i < "${#tmp[@]}"; i+=2 )) do
      if [[ ${tmp[i]} == "$2" ]]; then
        unset "tmp[i]" "tmp[i+1]"
        eval "hashtable$hash$1=(\"\${tmp[@]}\")"
        return
      fi
    done
  fi
}

# getkeys arrname
getkeys () {
  local i j LANG=C
  [[ $# == 1 && $1 =~ ^[[:alnum:]]+$ ]] || return 2
  return=()
  for i in "${!hashtable@}"; do
    [[ $i != hashtable????????????????????_"$1" ]] && continue
    eval "i=(\"\${$i[@]}\")"
    for (( j = 0; j < ${#i[@]}; j+=2 )) do
      return+=("${i[j]}")
    done
  done
}
