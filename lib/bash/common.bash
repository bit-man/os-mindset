#!/bin/bash

__prefix="$(basename "$0"):"

function showMsg() {
    echo "$__prefix: $1"
}

function showErr() {
    showMsg "${__prefix} ERROR: $*"
}

function testShow() {
    [[ -z $__EW_TEST ]] && return

    while [[ $1 ]]; do
        echo "${__prefix} TEST: $1"
        shift
    done
}

function OKorExit() {
	__rc=$?
    [[ $__rc -ne 0 ]] && \
    	showErr "$*" && testShow "STATUS=ERROR" && exit $__rc
}

function info() {
  echo "${__prefix} $*"
}

function spawn() {
    "$@"  > /dev/null 2>&1 &
}