#!/usr/bin/env bash

__osMindsetFolder=$(dirname "$0")
__debianLibFolder=${__osMindsetFolder}/../debian

# shellcheck disable=SC1091
source "${__osMindsetFolder}"/../../bin/lib.bash

declare -A optionRun=(
    [update]=update
    [suspend]=suspend
    [fan]=fanStop
    [vuln-tlsHello]=tlsHelloVulnerable
    [vbox-headers]=vboxNoStart
    [setkb]=setkb
    [work-mode]=work-mode
    [rest-mode]=rest-mode
    [docker-sudo]=docker-sudo
    [grub-hold]=grub-hold
    [sw-update]=sw-update
    [mono-uninstall]=mono-uninstall
    [ebury-detection]=ebury-detection
    [regreSSHion]=regreSSHion
    [blue-default]=blue-default
    [proton-vpn-restart]=proton-vpn-restart
    [notifications-off]=notifications-off
)

declare -A optionPrefix=(
    [update]=sudo
    [fan]=sudo
    [vbox-headers]=sudo
    [work-mode]=sudo
    [rest-mode]=sudo
    [docker-sudo]=sudo
    [grub-hold]=sudo
    [mono-uninstall]=sudo
    [regreSSHion]=sudo
)

declare -A optionHelp=(
    [update]="updates Debian install"
    [suspend]="suspend Ubuntu"
    [fan]="fixes \"Fan at full speed after suspend\" bug
          (https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1173997)"
    [vuln-tlsHello]="TLS Hello vulnerability verification"
    [vbox-headers]="fixes Virtual Box no start issue after kernel update"
    [setkb]="sets keyboard"
    [work-mode]="prepares CPU for working purposes (folding@home)"
    [rest-mode]="prepares CPU for rest purposes (folding@home)"
    [docker-sudo]="using docker without sudo"
    [grub-hold]="stop updating grub"
    [sw-update]="update git repositories"
    [mono-uninstall]="uninstall MS mono runtime"
    [ebury-detection]="Linux/Ebury backdoor detection (OpenSSH)"
    [regreSSHion]="Detects and adds workaround for openSSHion RCE"
    [blue-default]="Sets bluetooth as default sink for pulseaudio"
    [proton-vpn-restart]="Restarts Proton VPN"
    [notifications-off]="Gnome notificatons off"
)

## ~~-------------------------------------------------------- functions declaration

function usage() {
    ## Option listing
    echo -e "\nusage:"
    echo -n "    ${__PRG} ["
    for option in  "${!optionHelp[@]}"; do
        echo -n " ${option} |"
    done
    echo -e " ]\n"

    ## Options help
    for option in  "${!optionHelp[@]}"; do
        echo -e "    $option : ${optionHelp[${option}]}"
    done
    echo

}

## ~~------------------------------------------------------- Main

command=$1
shift

[[ "${command}" == "" || "${optionHelp[${command}]}" == "" ]] && \
    usage &&  exit 1

${optionPrefix[${command}]} "${__debianLibFolder}"/"${optionRun[${command}]}" "$@"
exit $?
