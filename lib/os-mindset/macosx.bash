#!/usr/bin/env bash

__osMindsetFolder=$(dirname $0)
__macosxLibFolder=${__osMindsetFolder}/../macosx
__bashLibFolder=${__osMindsetFolder}/../bash

source ${__osMindsetFolder}/../../bin/lib.bash
source ${__bashLibFolder}/assoc_arr.bash

setvalue optionRun update update
setvalue optionPrefix update ""
setvalue optionHelp update "updates Mac OS X install"

setvalue optionRun adoptopenjdk8-multiple-taps adoptopenjdk8-multiple-taps
setvalue optionPrefix adoptopenjdk8-multiple-taps ""
setvalue optionHelp adoptopenjdk8-multiple-taps "Fixes adoptopenjdk8 multiple taps"

setvalue optionRun copyq-sign copyq-sign
setvalue optionPrefix copyq-sign ""
setvalue optionHelp copyq-sign "Fixes M1 'CopyQ is damaged' error"

## ~~-------------------------------------------------------- functions declaration

function usage() {
    ## Option listing
    echo -e "\nusage:"
    echo -n "    ${__PRG} ["
    getkeys optionHelp
    for option in  ${return[*]}; do
        echo -n " ${option} |"
    done
    echo -e " ]\n"

    ## Options help
    getkeys optionHelp
    for option in  ${return[*]}; do
        getvalue optionHelp $option
        echo -e "    $option : ${return}"
    done
    echo

}

## ~~------------------------------------------------------- Main

command=$1
shift

getvalue optionHelp $command
[[ "${return}" == "" ]] && \
    usage &&  exit 1

getvalue optionPrefix $command
commandPrefix=${return}

getvalue optionRun $command
commandName=${return}

${commandPrefix} ${__macosxLibFolder}/${commandName} "$@"
exit $?
