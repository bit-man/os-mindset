#!/usr/bin/env bash

function main() {
    osType=`detectOS`

    command=$1
    shift

    while [ $# -gt 0 ]; do
        case $1 in
            --os)
                shift
                osType=$1
                ;;
            *)
                cmdParams="$cmdParams $1"
                ;;
        esac
        shift
    done
}
