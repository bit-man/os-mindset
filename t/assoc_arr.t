#!/usr/bin/env bash



thisFolder=$(dirname $0)
source ${thisFolder}/../lib/bash/assoc_arr.bash


testIsSetNoMapNameError() {
    isset
    assertEquals 2 $?
}

testIsSetNoKeyError() {
    isset map1
    assertEquals 2 $?
}

testIsNotSet() {
    isset map1 no-key
    assertEquals 1 $?
}

testIsSet() {
    setvalue map1 foo bar
    isset map1 foo
    assertEquals 0 $?
}


testSetValueNoMapNameError() {
    setvalue
    assertEquals 2 $?
}

testSetValueNoKeyError() {
    setvalue map1
    assertEquals 2 $?
}

testSetValueNovalueError() {
    setvalue map1 my-key
    assertEquals 2 $?
}

testGetValueNoMapNameError() {
    getvalue
    assertEquals 2 $?
}

testGetValueNoKeyError() {
    getvalue map1
    assertEquals 2 $?
}

testGetValue() {
    setvalue map1 foo2 bar2
    getvalue map1 foo2
    assertEquals 0 $?
    assertEquals 'bar2' $return
}


testUnsetValueNoMapNameError() {
    unsetvalue
    assertEquals 2 $?
}

testUnsetValueNoKeyError() {
    unsetvalue map1
    assertEquals 2 $?
}


testUnsetValueAlreadySet() {
    isset map1 foo3
    assertEquals 1 $?

    setvalue map1 foo3 bar3
    unsetvalue map1 foo3
    assertEquals 0 $?

    isset map1 foo3
    assertEquals 1 $?
}

testUnsetValueNotSetYet() {
    isset map1 foo4
    assertEquals 1 $?

    unsetvalue map1 foo4
    assertEquals 0 $?

    isset map1 foo4
    assertEquals 1 $?
}

testGetKeysFromEmptyIsEmpty() {
    getkeys map2
    assertEquals 'X' ${return}X

    isset map1 foo4
    assertEquals 1 $?
}

testGetKeys() {
    setvalue map3 foo1 bar1
    setvalue map3 foo2 bar2
    setvalue map3 foo3 bar3
    getkeys map3
    assertEquals 3 ${#return[@]}
    assertEquals 'foo1 foo2 foo3' "${return[*]}"

}

. shunit2
