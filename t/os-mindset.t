#!/usr/bin/env bash

__testFolder=$(dirname $0)

source ${__testFolder}/../bin/lib.bash
source ${__testFolder}/../lib/os-mindset/main.bash

function testCommandPassedIsStored() {
  main  someCommand
  assertEquals "someCommand" $command
}

function testPassedOSTypeTakesPrcedence() {
  main  someCommand --os MyOS
  assertEquals "MyOS" $osType
}

function testPassedCommandParams() {
  main  someCommand --param1 value1 --param2
  assertEquals " --param1 value1 --param2" "$cmdParams"
}

# Load shUnit2.
source shunit2
